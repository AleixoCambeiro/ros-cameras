#!/usr/bin/env python



# -------------------- Imports -------------------- #
import numpy
import rospy

import cv2

from sensor_msgs.msg import Image, CameraInfo
from cv_bridge import CvBridge, CvBridgeError

import camConn as cc

from threading import Thread, Lock, Event
import time
import yaml



# -------------------- Definitions -------------------- #
NODE_PREFIX		= "~"
COMMAND_PREFIX	= "_"
LEFT_PARAM		= "left"
RIGHT_PARAM		= "right"
ERROR_NO_CAM	= "Left and right camera parameters must be specified: stream " + COMMAND_PREFIX + LEFT_PARAM + ":=<cam_info_file> " + COMMAND_PREFIX + RIGHT_PARAM + ":=<cam_info_file>"

NODE_NAME		= "camStream"

RATE			= 30 # Publication rate is 30Hz

COLOR_ERR		= "\033[91m"
COLOR_END		= "\033[0m"


# Publisher parameters
QUEUE_SIZE		= 1

TOPIC_CAM		= "image_raw"
TOPIC_INFO		= "camera_info"


# Getting most recent frames
VCAP_L			= None
VCAP_R			= None

FRAME_L			= None
FRAME_R			= None

THREAD_SLEEP	= 0.01
THREAD_RUN		= Event()

FR_MUTEX_L		= Lock()
FR_MUTEX_R		= Lock()


# Camera calibration
CAM_CALIB_L		= None
CAM_CALIB_R		= None
ERROR_NO_CALIB	= "Calibration file could not be loaded from: "


# Format bridge for images
BRIDGE = CvBridge()




# -------------------- FUNCTIONS -------------------- #
def stream(left, right):
	global FRAME_L
	global FRAME_R
	
	global VCAP_L
	global VCAP_R
	
	streamInfo(left)
	streamInfo(right)
	
	pcl = camToPublisher(left)
	pcr = camToPublisher(right)
	
	pil = infoToPublisher(left)
	pir = infoToPublisher(right)
	
	rate = rospy.Rate(RATE)
	
	while not rospy.is_shutdown():
		#initTime = rospy.Time.now()
		
		FR_MUTEX_L.acquire()
		FR_MUTEX_R.acquire()
		
		stamp = rospy.Time.now()
		
		try:
			if FRAME_L is not None and FRAME_R is not None:
				msgCL = frameToMsg(FRAME_L, stamp)
				msgCR = frameToMsg(FRAME_R, stamp)
				
				msgIL = infoToMsg(CAM_CALIB_L, stamp)
				msgIR = infoToMsg(CAM_CALIB_R, stamp)
				
				pil.publish(msgIL)
				pir.publish(msgIR)
				
				pcl.publish(msgCL)
				pcr.publish(msgCR)
		finally:
			FR_MUTEX_L.release()
			FR_MUTEX_R.release()
		
		
		#print "Delay: ", (rospy.Time.now() - initTime).to_sec()
		
		rate.sleep()


def streamInfo(cam):
	print "STREAMING INFO:"
	print "Address: " + cam.toUrl()
	print "Camera topic: " + cam.topic + TOPIC_CAM
	print "Info topic: " + cam.topic + TOPIC_INFO
	print "Calibration file: " + cam.calibration
	#print "--------------------"
	#print CAM_CALIB
	print ""

def camToPublisher(cam):
	pub = rospy.Publisher(cam.topic + TOPIC_CAM, Image, queue_size=QUEUE_SIZE)
	
	return pub

def infoToPublisher(cam):
	pub = rospy.Publisher(cam.topic + TOPIC_INFO, CameraInfo, queue_size=QUEUE_SIZE)
	
	return pub

def frameToMsg(frame, stamp):
	msg = BRIDGE.cv2_to_imgmsg(frame, "bgr8")
	msg.header.stamp = stamp
	
	return msg

def infoToMsg(msg, stamp):
	msg.header.stamp = stamp
	
	return msg


# Initialization
def loadCalib(left, right):
	global CAM_CALIB_L
	global CAM_CALIB_R
	
	try:
		with file(left, 'r') as f:
			CAM_CALIB_L = yaml.load(f)
	except Exception:
		error(ERROR_NO_CALIB + left)
	
	try:
		with file(right, 'r') as f:
			CAM_CALIB_R = yaml.load(f)
	except Exception:
		error(ERROR_NO_CALIB + right)

def error(msg):
	print COLOR_ERR + msg + COLOR_END
	exit()

def init():
	global VCAP_L
	global VCAP_R
	
	global THREAD_RUN
	
	THREAD_RUN.set()
	
	rospy.init_node(NODE_NAME, anonymous=False)
	
	if not rospy.has_param(NODE_PREFIX + LEFT_PARAM) or not rospy.has_param(NODE_PREFIX + RIGHT_PARAM):
		error(ERROR_NO_CAM)
	
	infoL = rospy.get_param(NODE_PREFIX + LEFT_PARAM)
	infoR = rospy.get_param(NODE_PREFIX + RIGHT_PARAM)
	
	left  = cc.fromFile(infoL)
	right  = cc.fromFile(infoR)
	
	loadCalib(left.calibration, right.calibration)
	
	VCAP_L = cv2.VideoCapture(left.toUrl())
	VCAP_R = cv2.VideoCapture(right.toUrl())
	
	return left, right


# Read frames from buffer to get the last one
def doReadL():
	global FRAME_L
	global VCAP_L
	
	global THREAD_RUN
	
	while(THREAD_RUN.is_set()):
		FR_MUTEX_L.acquire()
		try:
			_, FRAME_L = VCAP_L.read()
		finally:
			FR_MUTEX_L.release()
		
		time.sleep(THREAD_SLEEP)

def doReadR():
	global FRAME_R
	global VCAP_R
	
	global THREAD_RUN
	
	while(THREAD_RUN.is_set()):
		FR_MUTEX_R.acquire()
		try:
			_, FRAME_R = VCAP_R.read()
		finally:
			FR_MUTEX_R.release()
		
		time.sleep(THREAD_SLEEP)


# -------------------- MAIN -------------------- #
if __name__ == '__main__':	
	left, right = init()
	
	tl = Thread(target = doReadL)
	tr = Thread(target = doReadR)
	
	tl.daemon = True
	tr.daemon = True
	
	tl.start()
	tr.start()
	
	try:
		stream(left, right)
	except KeyboardInterrupt:
		THREAD_RUN.clear()
	except rospy.ROSInterruptException:
		THREAD_RUN.clear()
		pass



