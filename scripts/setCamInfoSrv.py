#!/usr/bin/env python



# -------------------- Imports -------------------- #
import numpy
import rospy

from sensor_msgs.srv import SetCameraInfo, SetCameraInfoResponse

from sensor_msgs.msg import CameraInfo
import yaml



# -------------------- Definitions -------------------- #
NODE_NAME		= "setCamInfoSrv"

SRV_PREFIX		= None
SRV_NAME		= "set_camera_info"

OUTPUT_FILE		= None

INFO_SET		= "Camera info has been set successfully"
INFO_NOT_SET	= "Camera info could not be set"


# Node parameters
COLOR_ERR		= "\033[91m"
COLOR_SUC		= "\033[92m"
COLOR_END		= "\033[0m"

NODE_PREFIX		= "~"
COMMAND_PREFIX	= "_"
PREFIX_PARAM	= "pref"
OUT_PARAM		= "out"

ERROR_PARAMS	= "Service parameters must be specified: setCamInfoSrv " + COMMAND_PREFIX + PREFIX_PARAM + ":=<service_prefix> " + COMMAND_PREFIX + OUT_PARAM + ":=<output_file>"



# -------------------- FUNCTIONS -------------------- #
def setInfo(info):
	rsp = SetCameraInfoResponse()
	
	rsp.success = True
	rsp.status_message = INFO_SET
	
	calib = info.camera_info
	
	with open(OUTPUT_FILE, 'w') as outfile:
			yaml.dump(calib, outfile, default_flow_style=False)
	try:
		with open(OUTPUT_FILE, 'w') as outfile:
			yaml.dump(calib, outfile, default_flow_style=False)
		
		print "Configuration saved to: " , OUTPUT_FILE
	
	except Exception:
		rsp.success = False
		rsp.status_message = INFO_NOT_SET
		
		print rsp.status_message
	
	return rsp

def serviceInfo():
	success("Service listening at: " + SRV_PREFIX + SRV_NAME)

def doServe():
	s = rospy.Service(SRV_PREFIX + SRV_NAME, SetCameraInfo, setInfo)
	serviceInfo()
	
	rospy.spin()


def init():
	global SRV_PREFIX
	global OUTPUT_FILE
	
	rospy.init_node(NODE_NAME, anonymous=True)
	
	if not rospy.has_param(NODE_PREFIX + PREFIX_PARAM) or not rospy.has_param(NODE_PREFIX + OUT_PARAM):
		error(ERROR_PARAMS)
	
	SRV_PREFIX = rospy.get_param(NODE_PREFIX + PREFIX_PARAM)
	OUTPUT_FILE = rospy.get_param(NODE_PREFIX + OUT_PARAM)

def error(msg):
	print COLOR_ERR + msg + COLOR_END
	exit()

def success(msg):
	print COLOR_SUC + msg + COLOR_END



# -------------------- MAIN -------------------- #
if __name__ == '__main__':
	init()
	doServe()



