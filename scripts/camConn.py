import yaml

# Object (VO)
class Connection(object):
	
	def __init__(self, topic, protocol, camera, address, channel, calibration):
		self.topic = topic
		self.protocol = protocol
		self.camera = camera
		self.address = address
		self.channel = channel
		self.calibration = calibration
	
	def toUrl(self):
		url = self.protocol + self.address + self.camera + str(self.channel)
		return url

# Functions
def fromFile(fileName):
	with file(fileName, 'r') as f:
		cam = yaml.load(f)
		
	return cam
	
def toFile(fileName, camCon):
	with open(fileName, 'w') as outfile:
		yaml.dump(camCon, outfile, default_flow_style=False)
